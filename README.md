# Scouting Xilinx Drivers

*Important:* This package requires the cable driver installation files to be loated at `/opt/Xilinx/Vivado_Lab/[VIVADO_VERSION]/data/xicom/cable_drivers/lin64/install_script/install_drivers`.

Minimal RPM package that executes the Xilinx USB cable driver installation.

