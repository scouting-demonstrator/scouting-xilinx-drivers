%define debug_package %{nil}

Name: scouting-xilinx-drivers
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting wrapper RPM to install the Xilinx cable drivers
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
ExclusiveOs: linux
Provides: scouting-xilinx-drivers

Prefix: %{_prefix}

%description
Xilinx USB cable driver installation for CMS L1 Scouting.

%files

%prep

%build

%install

%clean

%pre

%post

# Install Xilinx USB drivers
cd /opt/Xilinx/Vivado_Lab/%{_vivadoversion}/data/xicom/cable_drivers/lin64/install_script/install_drivers
./install_drivers

%preun

%postun

