#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Please pass in the targeted Vivado version."
    echo "Example:"
    echo "   bash ${0} 2018.3"
    exit 1
fi
$VIVADOVERSION=$1

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BUILD_DIR="${DIR}/scouting-xilinx-drivers_rpmbuild"
cd $DIR
echo "Directory $DIR"

RAWVER=$(git describe --tags --long || echo 'v0.0.0-0')
VER=$(echo ${RAWVER} | sed 's/^v//' | awk '{split($0,a,"-"); print a[1]}') # If git describe fails, we set a default version number.
REL=$(echo "${RAWVER}" | sed 's/^v//' | awk '{split($0,a,"-"); print a[2]}')
TIM="$(date -u +"%F %T %Z")"
LOGINNAME="$(logname)"
PCR=${LOGINNAME:-${GITLAB_USER_LOGIN}_CI}

echo -n "{" >info.json
echo -n "\"version\": \"$VER\", " >>info.json
echo -n "\"release\": \"$REL\", " >>info.json
echo -n "\"time\": \"$TIM\", " >>info.json
echo -n "\"packager\": \"$PCR\"" >>info.json
echo -n "}" >>info.json

echo "Version $VER, release $REL"

RPM_OPTS=(--define "_topdir $BUILD_DIR" --define "_vivadoversion $VIVADOVERSION" --define "_version $VER" --define "_release $REL" --define "_packager $PCR")
rpmbuild "${RPM_OPTS[@]}" -bb package/package.spec
cp ${BUILD_DIR}/RPMS/x86_64/scouting-xilinx-drivers-* .

